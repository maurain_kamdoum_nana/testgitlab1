package echangecCientServer;

import javax.swing.JFrame;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.UnknownHostException;
import java.net.InetAddress;
import java.net.Socket;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextArea;


public class Client extends JFrame {
	public Client() {
		try {
			inet=InetAddress.getLocalHost();
		}catch(UnknownHostException e)
		{
			e.printStackTrace();
		}
		port=88;
		getContentPane().setLayout(null);
		
		
		JButton btnNewButton = new JButton("Connect");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
//				try {
////				socket=new Socket(inet.getHostName(), port);
//				}catch(IOException ioe)
//				{
//					ioe.printStackTrace();
//				}
			}
		});
		btnNewButton.setBounds(10, 11, 73, 23);
		getContentPane().add(btnNewButton);
		
		txtEcrivezVotreMessage = new JTextField();
		txtEcrivezVotreMessage.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txtEcrivezVotreMessage.setText(null);
			}
		});
		txtEcrivezVotreMessage.setText("Ecrivez votre message");
		txtEcrivezVotreMessage.setBounds(10, 108, 155, 20);
		getContentPane().add(txtEcrivezVotreMessage);
		txtEcrivezVotreMessage.setColumns(10);
		
		JButton btnNewButton_1 = new JButton("Send");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if((txtEcrivezVotreMessage.getText()!=null)) {
					String message="";
					try {
						socket=new Socket(inet.getHostName(), port);
						oos=new ObjectOutputStream(socket.getOutputStream());
						message=txtEcrivezVotreMessage.getText();
						oos.writeObject(message);
					} catch (IOException e1) {
						e1.printStackTrace();
						}
  				    textArea.append("Send: "+message+"\n");
//					textArea.setText("Send: "+message);
					txtEcrivezVotreMessage.setText("Ecrivez votre message");
					lblNewLabel.setText("Send: "+message);
					String Mmessage="";
					try {
						ois=new ObjectInputStream(socket.getInputStream());
						Mmessage=(String)ois.readObject();
						textArea.append("Received: "+Mmessage+"\n");
//						textArea.setText("Send: "+message+"\nReceived: "+Mmessage);
						lblNewLabel.setText("Send: "+message+"\nReceived: "+Mmessage);
					} catch (IOException e1) {
						e1.printStackTrace();
					} catch (ClassNotFoundException e2) {
						
						e2.printStackTrace();
					}
					finally {
						System.out.println(Mmessage);
					}
				}
		
				
			}
		});
		btnNewButton_1.setBounds(41, 149, 89, 23);
		getContentPane().add(btnNewButton_1);
		
		JPanel panel = new JPanel();
		panel.setBounds(234, 11, 227, 97);
		getContentPane().add(panel);
		
		lblNewLabel = new JLabel("");
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel, GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		panel.setLayout(gl_panel);
//		try {
//			ois=new ObjectInputStream(socket.getInputStream());
//			String Mmessage=(String)ois.readObject();
//			lblNewLabel.setText("Received: "+Mmessage);
//		} catch (IOException e1) {
//			e1.printStackTrace();
//		} catch (ClassNotFoundException e1) {
//			
//			e1.printStackTrace();
//		}
			
			JButton btnNewButton_2 = new JButton("Exit");
			btnNewButton_2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						oos.close();
						ois.close();
						socket.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					
				}
			});
			btnNewButton_2.setBounds(256, 149, 89, 23);
			getContentPane().add(btnNewButton_2);
			
			textArea = new JTextArea();
			textArea.setLineWrap(true);
//			textArea.setRows(20);
			textArea.setEditable(false);
			textArea.setBounds(41, 193, 199, 139);
			getContentPane().add(textArea);
			setBounds(30, 30, 600, 400);
			setVisible(true);
		
	}
	private int port;
	private InetAddress inet=null;
	private ObjectInputStream ois=null;
	private ObjectOutputStream oos=null;
	private Socket socket=null;
	private JTextField txtEcrivezVotreMessage;
	private JLabel lblNewLabel;
	private JTextArea textArea;
	

	public static void main(String[] args) {
		Client client=new Client();
		//Server1 serv = new Server1();

	}
}
